# -*- coding: utf-8 -*-


from yapsy.IPlugin import IPlugin
from includes.clipboard import ScryptoClip
from includes.helpers import column2string
from includes.ipython_clipboard_bsd import *

#допилировать наследие, лол, чтобы каждый плагин не создавал копию ScryptoClip, как-то так
class ColumnToUnQuotedList(IPlugin):
	def __init__(s): #для краткости вместо ссылки на инстанс "self" в плагинах используем "s"
		#s.c=ScryptoClip()
		s.nameLocale = 'Колонки в строку без кавычек'
		pass
	def doWork(s,arg):
		#a="a\nc\nd\nf"
		#print column2string(a)
		#clipBoardContents=
		try:
			print 1
			#clipboard_get()
			print 3
			clipboard_set(column2string( clipboard_get() ))
			print 2
			#s.c.put(column2string( s.c.get() ))
		except:	
			#@todo: допилировать нормальное логирование
			print sys.exc_info()
	#здесь будет функция, которая экспортит во фронтэнд методы, которые предоставляет плагин
	#чтобы не делать по плагину на каждую маленькую хуитку
	def exportMethods(s):
		pass
	
	#@fix, @delete
	# не нашел другого способа передачи имени, вообще по-хорошему надо это хранить в описанни к плагину
	# в описании к плагину это вызывается через  pluginInfo.description и pluginInfo.name, соответственно, нам не нужно здесь 
	# дублировать этот функционал
	#def getName(s):
	#	return 'Колонки в строку без кавычек'
	# @fix end
