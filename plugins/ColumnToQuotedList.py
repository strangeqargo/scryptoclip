# -*- coding: utf-8 -*-


from yapsy.IPlugin import IPlugin
from includes.clipboard import ScryptoClip
from includes.helpers import column2string
from includes.ipython_clipboard_bsd import *


#допилировать наследие, лол, чтобы каждый плагин не создавал копию ScryptoClip, как-то так
class ColumnToQuotedList(IPlugin):
	def __init__(s): #для краткости вместо ссылки на инстанс "self" в плагинах используем "s"
		s.nameLocale = 'Колонки в строку с кавычками'
		pass
	def doWork(s,arg):
		try:
			clipboard_set(column2string( clipboard_get(), '"' ))
		except:	
			#@todo: допилировать нормальное логирование
			print sys.exc_info()[0]
	#здесь будет функция, которая экспортит во фронтэнд методы, которые предоставляет плагин
	#чтобы не делать по плагину на каждую маленькую хуитку
	def exportMethods(s):
		pass
	
	#не нашел другого способа передачи имени, вообще по-хорошему надо это хранить в описанни к плагину
	def getName(s):
		return 'Колонки в строку с кавычками'
		
		
		
