# -*- coding: utf-8 -*-

"""
Taken from the IPython project http://ipython.org

Used under the terms of the BSD license
"""

import subprocess
import sys

def clipboard_get():
	print sys.platform
	""" Get text from the clipboard.
	"""
	try:
		from gi.repository import Gtk
		return pygobject_clipboard_get()
	except:
		#@todo
		print 'nogtk_get'
	try: 
		from PyQt4 import QtGui
		
		return pyqt_clipboard_get()
	except:
		#@todo
		print 'noqt'
		pass
		
	if sys.platform == 'win32':
		try:
			return win32_clipboard_get()
		except Exception:
			pass
	elif sys.platform == 'darwin':
		try:
			return osx_clipboard_get()
		except Exception:
			pass
	return tkinter_clipboard_get()


def clipboard_set(text):
	#return 1
	""" Get text from the clipboard.
	"""
	#try:
	from gi.repository import Gtk
	return pygobject_clipboard_set(text)
	#except:
		#@todo
	#	print sys.exc_info()
	#	print 'nogtk_set'
	try: 
		from PyQt4 import QtGui
		
		return pyqt_clipboard_set(text)
	except:
		pass
		
	if sys.platform == 'win32':
		try:
			return win32_clipboard_set(text)
		except Exception:
			raise
	elif sys.platform == 'darwin':
		try:
			return osx_clipboard_set(text)
		except Exception:
			pass
	xsel_clipboard_set(text)
	
def pygobject_clipboard_get():
	try:
		from gi.repository import Gtk 
		from gi.repository import Gdk 
		#Gtk.Clipboard
		clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
		text = clipboard.wait_for_text()
		print 'gtk teeeext'
		print text
		return text
	except:
		#@todo
		print sys.exc_info()
		print 'gtk error'
		
def pygobject_clipboard_set(text):
	#try:
	from gi.repository import Gtk
	from gi.repository import Gdk 
	#Gtk.Clipboard
	clipboard=Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
	clipboard.set_text(text, -1)
	#except:
	#@todo
	#print sys.exc_info()
	#print 'gtk paste error'
			
def win32_clipboard_get():
	""" Get the current clipboard's text on Windows.

	Requires Mark Hammond's pywin32 extensions.
	"""
	try:
		import win32clipboard
	except ImportError:
		message = ("Getting text from the clipboard requires the pywin32 "
			"extensions: http://sourceforge.net/projects/pywin32/")
		raise Exception(message)
	win32clipboard.OpenClipboard()
	#text = win32clipboard.GetClipboardData(win32clipboard.CF_TEXT).decode('utf-8')
	#text = win32clipboard.GetClipboardData(win32clipboard.CF_TEXT).decode('utf-8')
	text = win32clipboard.GetClipboardData(win32clipboard.CF_UNICODETEXT)#.decode('utf-8')
	# FIXME: convert \r\n to \n?
	win32clipboard.CloseClipboard()
	return text


def osx_clipboard_get():
	""" Get the clipboard's text on OS X.
	"""
	p = subprocess.Popen(['pbpaste', '-Prefer', 'ascii'],
		stdout=subprocess.PIPE)
	text, stderr = p.communicate()
	# Text comes in with old Mac \r line endings. Change them to \n.
	text = text.replace('\r', '\n')
	return text


def tkinter_clipboard_get():
	""" Get the clipboard's text using Tkinter.

	This is the default on systems that are not Windows or OS X. It may
	interfere with other UI toolkits and should be replaced with an
	implementation that uses that toolkit.
	"""
	try:
		import Tkinter
	except ImportError:
		message = ("Getting text from the clipboard on this platform "
			"requires Tkinter.")
		raise Exception(message)
	root = Tkinter.Tk()
	root.withdraw()
	text = root.clipboard_get()
	root.destroy()
	return text


def win32_clipboard_set(text):
	# idiosyncratic win32 import issues
	import pywintypes as _
	import win32clipboard
	import win32con
	win32clipboard.OpenClipboard()
	try:
		win32clipboard.EmptyClipboard()
		#win32clipboard.SetClipboardText(_fix_line_endings(text))
		win32clipboard.SetClipboardData(win32con.CF_UNICODETEXT, _fix_line_endings(text).encode('utf-8') )
	finally:
		win32clipboard.CloseClipboard()

def pyqt_clipboard_get():
	from PyQt4 import QtGui
	print "GET QT4"
	clip = QtGui.QApplication.clipboard()
	print "------>"
	#print clip.text()
	#print text	
	print "<------"
	return str(clip.text()).decode('utf-8')

def pyqt_clipboard_set(text):
	from PyQt4 import QtGui
	print "SET QT4"
	clip = QtGui.QApplication.clipboard()
	clip.setText(text)

	
def _fix_line_endings(text):
	return '\r\n'.join(text.splitlines())


def osx_clipboard_set(text):
	""" Get the clipboard's text on OS X.
	"""
	p = subprocess.Popen(['pbcopy', '-Prefer', 'ascii'],
		stdin=subprocess.PIPE)
	p.communicate(input=text)


def xsel_clipboard_set(text):
	from subprocess import Popen, PIPE
	p = Popen(['xsel', '-bi'], stdin=PIPE)
	p.communicate(input=text)
	

