# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright 2009-2012 Canonical Ltd.
#
# Authors: Neil Jagdish Patel <neil.patel@canonical.com>
#          Jono Bacon <jono@ubuntu.com>
#          David Planella <david.planella@ubuntu.com>
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of either or both of the following licenses:
#
# 1) the GNU Lesser General Public License version 3, as published by the 
# Free Software Foundation; and/or
# 2) the GNU Lesser General Public License version 2.1, as published by 
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the applicable version of the GNU Lesser General Public 
# License for more details.
#
# You should have received a copy of both the GNU Lesser General Public 
# License version 3 and version 2.1 along with this program.  If not, see 
# <http://www.gnu.org/licenses/>
#

from gi.repository import Gtk
from gi.repository import AppIndicator3 as appindicator

class SystemTrayIcon:

	def __init__(s, plugMan):
		s.plugMan=plugMan
		ind = appindicator.Indicator.new ("example-simple-client", "indicator-messages", appindicator.IndicatorCategory.APPLICATION_STATUS)
		ind.set_status (appindicator.IndicatorStatus.ACTIVE)
		ind.set_attention_icon ("indicator-messages-new")
		s.menu = Gtk.Menu()
		s.createTrayMenu()
		#for i in range(3):
		#	buf = "Test-undermenu - %d" % i

		#	menu_items = Gtk.MenuItem(buf)

		#	s.menu.append(menu_items)

			# this is where you would connect your menu item up with a function:
			
			# menu_items.connect("activate", menuitem_response, buf)

			# show the items
		#	menu_items.show()
		
		ind.set_menu(s.menu)
		Gtk.main()
		pass
	
	def createTrayMenu(s):
		for pluginInfo in s.plugMan.getAllPlugins():
			s.plugMan.activatePluginByName(pluginInfo.name)
			#@todo: попозже мы сделаем чтобы один плагин мог экспортить несколько функций.
			# try:
				# for method in pluginInfo.plugin_object.exportMethods():
					# self.createMenuEntry(pluginInfo.name, pluginInfo.plugin_object.method)
			# except:
				#pass
			s.createMenuEntry(pluginInfo.name, 	pluginInfo.plugin_object.doWork, pluginInfo.name)
			print(pluginInfo.name)
			print(pluginInfo.description)
		s.createMenuEntry('Exit', Gtk.main_quit)	
		pass
	
	def createMenuEntry(s, name, function, menuTitle=None):
		if not menuTitle:
			menuTitleText=name
		else:
			menuTitleText = menuTitle
		menu_items = Gtk.MenuItem(menuTitleText)
		menu_items.connect('activate', function)
		print function
		s.menu.append(menu_items)
		print s.menu
		menu_items.show()		
		#from qt:
		#self.actions[name]=QtGui.QAction(menuTitleText.decode('utf-8', 'ignore'), self)
		#self.menu.addAction(self.actions[name])
		#self.actions[name].triggered.connect(function)

	def menuitem_response(w, buf):
		print buf


#if __name__ == "__main__":


# create a menu


# create some 


