# -*- coding: utf-8 -*-
from PyQt4 import QtGui
class SystemTrayIcon(QtGui.QSystemTrayIcon):

	def __init__(self, icon, plugMan, storageMan, parent=None):
		QtGui.QSystemTrayIcon.__init__(self, icon, parent)
		#self.activated.connect(self.pr())
		self.menu = QtGui.QMenu(parent)
		self.actions={}
		#разобраться, как иконки выставляются
		#... оверлоадингом функций по параметрам они выставляются :-)
		#setIcon(Icon) все просто =), нужно как-то научить прокидывать из плагина всякие доп. параметры: локализованное название, иконку, шорткат
		self.plugMan=plugMan
		
		# здесь мы пока используем дефолтный tk-клипборд, его мы ПРЯМЩАС ПОПИЛИМ НА ЧАСТИ, 
		# чтобы можно было использовать qt-шные или системные функции доступа к клипборду
		# для этого надо будет распилировать класс ScryptoClip
		
		self.createTrayMenu()
        #системные пункты меню, отделяем сепаратором
		self.menu.addSeparator()
		#self.createMenuEntry(MENU_ACTION_PREF_TEXT, parent.show)
		#self.createMenuEntry(MENU_ACTION_EXIT_TEXT, QtGui.qApp.quit)
		self.createMenuEntry('exit', QtGui.qApp.quit)
		self.setContextMenu(self.menu)
	def pr(self):
		print 1
		return True
	
	
	def createTrayMenu(self):
		for pluginInfo in self.plugMan.getAllPlugins():
			self.plugMan.activatePluginByName(pluginInfo.name)
			#@todo: попозже мы сделаем чтобы один плагин мог экспортить несколько функций.
			# try:
				# for method in pluginInfo.plugin_object.exportMethods():
					# self.createMenuEntry(pluginInfo.name, pluginInfo.plugin_object.method)
			# except:
				#pass
			self.createMenuEntry(pluginInfo.name,
								 pluginInfo.plugin_object.doWork,
								 pluginInfo.name)
			print(pluginInfo.name)
			print(pluginInfo.description)
		pass
	
	def createMenuEntry(self, name, function, menuTitle=None):
		if not menuTitle:
			menuTitleText=name
		else:
			menuTitleText = menuTitle
		self.actions[name]=QtGui.QAction(menuTitleText.decode('utf-8', 'ignore'), self)
		#@delete
		#exitAction.setShortcut('Ctrl+Q')
		#exitAction.setStatusTip('Exit')
		#exitAction.triggered.connect(QtGui.qApp.quit)
		#print self.actions
		#sys.exit()
		#@delete end
		self.menu.addAction(self.actions[name])
		self.actions[name].triggered.connect(function)

#класс окошка настроек
#не знаю где его хранить, пока пусть живет здесь, потом можно вынести в плагин наверное.
class PreferencesWindow(QtGui.QWidget):
    
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('ScriptoClip: Preferences')
        self.setWindowIcon(QtGui.QIcon(applicationPath+ICON_TRAY_PATH))
	
    def closeEvent(self, event):
        event.ignore()
        self.hide()