# -*- coding: utf-8 -*-
from yapsy.PluginManager import PluginManager

# Build the manager
plugMan = PluginManager()
# Tell it the default place(s) where to find plugins
plugMan.setPluginPlaces(["plugins"])
# Load all plugins
plugMan.collectPlugins()

# Activate all loaded plugins
for pluginInfo in plugMan.getAllPlugins():
	#plugMan.activatePluginByName(pluginInfo.name)
	#print(pluginInfo.name)
	#pluginInfo.plugin_object.doWork()
	pass


